<?php

/*
 * This file is part of the overtrue/easy-sms.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace End01here\EasySms\Gateways;


use End01here\EasySms\Contracts\MessageInterface;
use End01here\EasySms\Contracts\PhoneNumberInterface;
use End01here\EasySms\Exceptions\GatewayErrorException;
use End01here\EasySms\Traits\HasHttpRequest;

/**
 * Class AliyunGateway.
 *
 * @author carson <docxcn@gmail.com>
 *
 * @see https://help.aliyun.com/document_detail/55451.html
 */
class PuXinGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_URL = 'http://47.106.229.82:8888/sms.aspx';

    const ENDPOINT_TYPE = '0';

    public function send(PhoneNumberInterface $to, MessageInterface $message )
    {
        //获取签名
        $signName = $this->config['sign_text'];

        //组装请求数据
        $msg="【{$signName}】".$message->getContent();
        $post_data = "account=". $this->config['px']['username']
            ."&password=".$this->config['px']['password']
            ."&userid=".$this->config['px']['userid']
            ."&mobile=". $to->getNumber()
            ."&content=".urlencode($msg)
            ."&sendTime=&action=send&extno=";
        //发起请求
        $result = $this->post(self::ENDPOINT_URL,$post_data);
        $result = json_decode(json_encode(simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        //解析返回信息sadasdsfdsf
        if (isset($result['status']) && $result['status']!='Success') {
            throw new GatewayErrorException($result['message'], 0, $result);
        }

        return ['code'=>'1','msg'=>'短信发送成功'];
    }



}
