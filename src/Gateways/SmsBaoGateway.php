<?php

/*
 * This file is part of the overtrue/easy-sms.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace End01here\EasySms\Gateways;


use End01here\EasySms\Contracts\MessageInterface;
use End01here\EasySms\Contracts\PhoneNumberInterface;
use End01here\EasySms\Traits\HasHttpRequest;

/**
 * Class 短信宝短信发送.
 *
 * @author carson <docxcn@gmail.com>
 *
 * @see https://help.aliyun.com/document_detail/55451.html
 */
class SmsBaoGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_URL = 'http://api.smsbao.com/sms';


    public function send(PhoneNumberInterface $to, MessageInterface $message )
    {
        //获取签名
        $signName = $this->config['sign_text'];

        //组装请求数据
        $msg=$message->getContent();
//        $sendtime = time()*1000;
//        $sign=$this->sign($msg,$sendtime);
        $post_data = [
            'u'=>$this->config['dx']['username'],
            'p'=>md5($this->config['dx']['password']),
            'c'=>'【'.$signName.'】'.$msg,
            'm'=>$to->getNumber(),
        ];
        //发起请求
//        var_dump(self::ENDPOINT_URL.'?'.http_build_query($post_data));
//        exit;
        $result = $this->post(self::ENDPOINT_URL.'?'.http_build_query($post_data));
//        dd($result);
        //解析返回信息sadasdsfdsf
//        if (isset($result['code']) && $result['code']!=0) {
//            throw new GatewayErrorException('短信发送失败', -1, $result);
//        }

        return ['code'=>'1','msg'=>'短信发送成功'];
    }

    function sign($msg,$sendtime){
        $post_data = $this->config['wddz']['username'].$msg.$sendtime.md5($this->config['wddz']['password']);
        return md5($post_data);
    }



}
