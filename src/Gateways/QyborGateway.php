<?php

/*
 * This file is part of the overtrue/easy-sms.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace End01here\EasySms\Gateways;


use End01here\EasySms\Contracts\MessageInterface;
use End01here\EasySms\Contracts\PhoneNumberInterface;
use End01here\EasySms\Exceptions\GatewayErrorException;
use End01here\EasySms\Traits\HasHttpRequest;

/**
 * Class AliyunGateway.
 *
 * @author carson <docxcn@gmail.com>
 *
 * @see https://help.aliyun.com/document_detail/55451.html
 */
class QyborGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_URL = 'http://www.qybor.com:8500/shortMessage';

    const ENDPOINT_TYPE = '0';

    public function send(PhoneNumberInterface $to, MessageInterface $message )
    {
        //获取签名
        $signName = $this->config['sign_text'];

        //组装请求数据
        $msg="【{$signName}】".$message->getContent();
        $port = '';
        $sendtime = date('Y-m-d H:i:s');
        $post_data = "username=". $this->config['qyb']['username']
            ."&passwd=".$this->config['qyb']['password']
            ."&phone=". $to->getNumber()
            ."&msg=".urlencode($msg)
            ."&needstatus=true&port=".$port
            ."&sendtime=".$sendtime;
        //发起请求
        $result = $this->post(self::ENDPOINT_URL,$post_data);
        //解析返回信息sadasdsfdsf
        if (isset($result['respcode']) && $result['respcode']!=0) {
            throw new GatewayErrorException($result['respdesc'], $result['respcode'], $result);
        }

        return ['code'=>'1','msg'=>'短信发送成功'];
    }



}
