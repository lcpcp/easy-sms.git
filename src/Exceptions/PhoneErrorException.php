<?php

/*
 * This file is part of the overtrue/easy-sms.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace End01here\EasySms\Exceptions;

/**
 * Class GatewayErrorException.
 */
class PhoneErrorException extends Exception
{


    /**
     * GatewayErrorException constructor.
     *
     * @param string $message
     * @param int    $code
     */
    public function __construct($message, $code=0)
    {
        parent::__construct($message, intval($code));

    }
}
