<?php

return [
    //code码验证存储方式
    'cache_type' => env('CACHE_SMS_TYPE','file'),
    //短信有效时长（分钟）
    'cache_time' => env('CACHE_SMS_TIME','15'),
    //短信最短重发时间（秒）
    'mix_time' => env('CACHE_MIX_TIME','60'),
    //签名信息
    'sign_text'=>env('SIGN_TEXT',''),
    //请求超时时间
    'timeout'=>env('SMS_TIMEOUT',''),
    //是否开启通用验证码
    'is_current'=>env('IS_CURRENT',false),
    //通用验证码
    'current_code'=>env('CURRENT_CODE','7453'),
    //kewail平台短信配置信息
    'kewail' => [
        'accesskey' => env('KEWAIL_ACCESSKEY',''),
        'secretkey' => env('KEWAIL_SECRETKEY',''),
    ],
    //阿里云短信配置
    'ali' => [
        'access_key_id' => env('ALI_ACCESS_KEY_ID',''),
        'secretkey' => env('ALI_ACCESS_KEY',''),
    ],
    //企业宝短信配置
    'qyb' => [
        'username' => env('QYB_USERNAME',''),
        'password' => env('QYB_PASSWORD',''),
    ],
    //蒲公英短信
    'px' => [
        'username' => env('PX_USERNAME',''),
        'password' => env('PX_PASSWORD',''),
        'userid' => env('PX_USERID',''),
    ],
    //蔚岛云短信
    'wddz' => [
        'username' => env('WDDZ_USERNAME',''),
        'password' => env('WDDZ_PASSWORD',''),
    ],
    //短信短信
    'dx' => [
        'username' => env('DX_USERNAME',''),
        'password' => env('DX_PASSWORD',''),
    ]
];
