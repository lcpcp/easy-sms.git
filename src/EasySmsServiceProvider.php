<?php

namespace End01here\EasySms;

use End01here\EasySms\Console\ClearFileCodeCommand;
use Illuminate\Support\ServiceProvider;

class EasySmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //注册清除服务
        $this->commands([
            ClearFileCodeCommand::class
        ]);

        // 单例绑定服务
//        $this->app->singleton('command.eternaltree.install', function ($app) {
//            return new ClearFileCodeCommand( );
//        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
//        $this->loadViewsFrom(__DIR__ . '/views', 'Packagetest'); // 视图目录指定
        $this->publishes([
//            __DIR__.'/views' => base_path('resources/views/vendor/packagetest'),  // 发布视图目录到resources 下
            __DIR__.'/Config/easy_sms.php' => config_path('easy_sms.php'), // 发布配置文件到 laravel 的config 下
        ]);
    }
}
